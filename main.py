#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import logging
import os
import sys

logging.debug(sys.path)
if 'feedparser' not in sys.path:
    sys.path.append(os.path.join(os.path.dirname(__file__), 'lib/feedparser'))

import feedparser
import jinja2
import webapp2
from google.appengine.ext import db
from google.appengine.api import users
from handlers import feed_category
from handlers import feed
from handlers import userprefs

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

class MainHandler(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        results = db.GqlQuery("SELECT * FROM Feed WHERE user_id = :1", user.user_id())
        feed_obj = results.get()

        atomxml = feedparser.parse(feed_obj.feed_url)
        entries = atomxml['entries']
        feed = atomxml['feed']



        username = ''
        is_logged_in = False
        if user:
            is_logged_in = True
            username = user.nickname()
        else:
            self.redirect(users.create_login_url(self.request.uri))

        template_values = { 
            "entries" : entries,
            "feed" : feed,
            "username" : username,
            "is_logged_in" : is_logged_in,
            "logout_url" : users.create_logout_url('/')
        }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

class LoginHandler(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        username = ''
        is_logged_in = False
        if user:
            is_logged_in = True
            username = user.nickname()

        template_values = { "username" : username, "is_logged_in" : is_logged_in }
        template = JINJA_ENVIRONMENT.get_template('login.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/feeds', feed.FeedListHandler),
    ('/feeds/new', feed.NewFeedHandler),
    ('/login', LoginHandler),
    ('/userprefs', userprefs.UserPrefListHandler),
    ('/userprefs/new', userprefs.NewUserPrefHandler),
    ('/feed_category', feed_category.FeedCategoryHandler)
], debug=True)
