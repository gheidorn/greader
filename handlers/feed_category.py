#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import datetime
import json
import logging
import os
import webapp2
from google.appengine.ext import db
from google.appengine.api import users

# JSON encoder for datetime
def handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))

# object representing a feed
class FeedCategory(db.Model):
    """Models an individual FeedCategory"""
    name = db.StringProperty()
    description = db.StringProperty()
    user_id = db.StringProperty()
    stored_dt = db.DateTimeProperty(auto_now_add=True)


class FeedCategoryHandler(webapp2.RequestHandler):
    # GET /feed_category
    # user must be logged in
    def get(self):
        user = users.get_current_user()
        results = db.GqlQuery("SELECT * FROM FeedCategory WHERE user_id = :1", user.user_id())
        feed_categories = []
        for fc in results:
            feed_categories.append(db.to_dict(fc))
        response_json = { "feed_categories" : feed_categories }
        self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
        self.response.write(json.dumps(response_json, default=handler))

    # PUT /feed_category
    # user must be logged in
    def put(self):
        user = users.get_current_user()

        name = self.request.get('name')
        description = self.request.get('description')
        user_id = user.user_id()

        fc = FeedCategory()
        fc.name = name
        fc.description = description
        fc.user_id = user_id
        fc.put()

        response_json = { 
            "status" : 200,
            "message" : "Successfully PUT new FeedCategory",
            "feed_category" : {
                "name" : fc.name,
                "description" : fc.description,
                "user_id" : fc.user_id,
                "stored_dt" : fc.stored_dt
            },
            "user" : { 
                "user_id" : user.user_id(), 
                "username" : user.nickname(), 
                "email" : user.email() 
            } 
        }
        self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
        self.response.write(json.dumps(response_json, default=handler))