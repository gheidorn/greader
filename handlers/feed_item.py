#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import datetime
from google.appengine.ext import db
import jinja2
import json
import logging
import os
import time
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

# JSON encoder for datetime
def handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))

# object representing a feed
class FeedItem(db.Model):
    """Models an individual FeedItem"""
    title = db.StringProperty()
    summary_detail = db.StringProperty()
    author = db.StringProperty()
    parsed_dt = db.DateTimeProperty()
    published_dt = db.DateTimeProperty()
    stored_dt = db.DateTimeProperty(auto_now_add=True)

# handler for /feed/:id URI
# use Accept header application/json to access resource
class FeedItemListHandler(webapp2.RequestHandler):
    def get(self):
        results = db.GqlQuery("SELECT * FROM Feed")
        logging.debug("content type " + self.request.headers['Accept'])

        #determine response based on Content Type requested
        if self.request.headers['Accept'] == 'application/json':
            logging.debug("presenting data for resource: /feeds")
            feeds = []
            for feed in results:
                feeds.append(db.to_dict(feed))
            self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
            self.response.write(json.dumps(feeds, default=handler))
        else:
            logging.debug("presenting feed list view to /feeds")
            feeds = []
            for feed in results:
                feeds.append(feed)
            template_values = { 'feeds' : feeds }
            template = JINJA_ENVIRONMENT.get_template('feeds/list.html')
            self.response.write(template.render(template_values))

# handler for /feeds/new URI
class NewFeedHandler(webapp2.RequestHandler):
    def get(self):
        logging.debug("presenting feed form to /feeds/new")
        template_values = { 'feed' : Feed() }
        template = JINJA_ENVIRONMENT.get_template('feeds/new.html')
        self.response.write(template.render(template_values))

    def post(self):
        logging.info("posting feed form data to /feeds/new")

        result  = self.validateNewFeedData()
        if len(result['errors']) > 0:
            template_values = {
                'message' : "There was a problem with your submission:",
                'messageType' : "error",
                'errors' : result['errors'],
                'feed' : result['feed']
            }
        else:
            logging.info('saving validated new feed')
            result['feed'].put()
            template_values = {
                'message' : "New feed created successfully!",
                'messageType' : "success",
                'feed' : result['feed']
            }

        template = JINJA_ENVIRONMENT.get_template('feeds/new.html')
        self.response.write(template.render(template_values))

    def validateNewFeedData(self):
        logging.info("validating new feed data")
        errors = []

        # basic form validation
        
        feed_name = self.request.get('feedName')
        if feed_name == '':
            errors.append({ 'field' : 'username', 'message' : 'Username cannot be blank.'})
        first_name = self.request.get('firstName')
        if first_name == '':
            errors.append({ 'field' : 'firstName', 'message' : 'First Name cannot be blank.'})
        last_name = self.request.get('lastName')
        if last_name == '':
            errors.append({ 'field' : 'lastName', 'message' : 'Last Name cannot be blank.'})
        role = self.request.get('role')
        
        if len(errors) == 0:
            # check datastore for existing username
            query = db.GqlQuery("SELECT * FROM User WHERE username = :1", username)
            existing_user = query.get()
            if existing_user and existing_user.username == username:
                errors.append({ 'field' : 'username', 'message' : 'Username already taken.  Please choose another.'})

        # assemble new user
        user = User()
        user.username = username
        user.first_name = first_name
        user.last_name = last_name
        user.role = role
        
        return { 'errors' : errors, 'feed' : feed }