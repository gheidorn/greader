#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import datetime
from google.appengine.ext import db
import jinja2
import json
import logging
import os
import time
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

# JSON encoder for datetime
def handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))

# object representing a userpref
class UserPref(db.Model):
    """Models a set of user preferences"""
    username = db.StringProperty()
    first_name = db.StringProperty()
    last_name = db.StringProperty()
    role = db.StringProperty()
    created_dt = db.DateTimeProperty(auto_now_add=True)

# handler for /userprefs URI
# use Accept header application/json to access resource
class UserPrefListHandler(webapp2.RequestHandler):
    def get(self):
        results = db.GqlQuery("SELECT * FROM UserPref")
        logging.debug("content type " + self.request.headers['Accept'])

        #determine response based on Content Type requested
        if self.request.headers['Accept'] == 'application/json':
            logging.debug("presenting data for resource: /userprefs")
            userprefs = []
            for userpref in results:
                userprefs.append(db.to_dict(userpref))
            self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
            self.response.write(json.dumps(userprefs, default=handler))
        else:
            logging.debug("presenting userpref list view to /userprefs")
            userprefs = []
            for userpref in results:
                userprefs.append(userpref)
            template_values = { 'userprefs' : userprefs }
            template = JINJA_ENVIRONMENT.get_template('userprefs/list.html')
            self.response.write(template.render(template_values))

# handler for /userprefs/new URI
class NewUserPrefHandler(webapp2.RequestHandler):
    def get(self):
        logging.debug("presenting userpref form to /userprefs/new")
        template_values = { 'userpref' : UserPref() }
        template = JINJA_ENVIRONMENT.get_template('userprefs/new.html')
        self.response.write(template.render(template_values))

    def post(self):
        logging.info("posting userpref form data to /userprefs/new")

        result  = self.validateNewUserPrefData()
        if len(result['errors']) > 0:
            template_values = {
                'message' : "There was a problem with your submission:",
                'messageType' : "error",
                'errors' : result['errors'],
                'userpref' : result['userpref']
            }
        else:
            logging.info('saving validated new userpref')
            result['userpref'].put()
            template_values = {
                'message' : "New userpref created successfully!",
                'messageType' : "success",
                'userpref' : result['userpref']
            }

        template = JINJA_ENVIRONMENT.get_template('userprefs/new.html')
        self.response.write(template.render(template_values))

    def validateNewUserPrefData(self):
        logging.info("validating new userpref data")
        errors = []

        # basic form validation
        username = self.request.get('username')
        if username == '':
            errors.append({ 'field' : 'username', 'message' : 'Username cannot be blank.'})
        first_name = self.request.get('firstName')
        if first_name == '':
            errors.append({ 'field' : 'firstName', 'message' : 'First Name cannot be blank.'})
        last_name = self.request.get('lastName')
        if last_name == '':
            errors.append({ 'field' : 'lastName', 'message' : 'Last Name cannot be blank.'})
        role = self.request.get('role')
        
        if len(errors) == 0:
            # check datastore for existing username
            query = db.GqlQuery("SELECT * FROM UserPref WHERE username = :1", username)
            existing_userpref = query.get()
            if existing_userpref and existing_userpref.username == username:
                errors.append({ 'field' : 'username', 'message' : 'Username already taken.  Please choose another.'})

        # assemble new userpref
        userpref = UserPref()
        userpref.username = username
        userpref.first_name = first_name
        userpref.last_name = last_name
        userpref.role = role

        return { 'errors' : errors, 'userpref' : userpref }