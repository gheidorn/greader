#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import datetime
import jinja2
import json
import logging
import os
import time
import webapp2
from google.appengine.ext import db
from google.appengine.api import users

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

# JSON encoder for datetime
def handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))

# object representing a feed
class Feed(db.Model):
    """Models an individual Feed"""
    name = db.StringProperty()
    feed_url = db.StringProperty()
    site_url = db.StringProperty()
    user_id = db.StringProperty()
    stored_dt = db.DateTimeProperty(auto_now_add=True)

# object representing a feed
class FeedItem(db.Model):
    """Models an individual FeedItem"""
    title = db.StringProperty()
    summary_detail = db.StringProperty()
    author = db.StringProperty()
    parsed_dt = db.DateTimeProperty()
    published_dt = db.DateTimeProperty()
    stored_dt = db.DateTimeProperty(auto_now_add=True)

# handler for /feeds URI
# use Accept header application/json to access resource
class FeedListHandler(webapp2.RequestHandler):
    def get(self):
        feed_results = db.GqlQuery("SELECT * FROM Feed")
        logging.debug("content type " + self.request.headers['Accept'])

        #determine response based on Content Type requested
        if self.request.headers['Accept'] == 'application/json':
            logging.debug("presenting data for resource: /feeds")
            feeds = []
            for feed in feed_results:
                feed_item_results = db.GqlQuery("SELECT * FROM FeedItem WHERE feed_id = :1", feed.feed_id)
                feeds.append(db.to_dict(feed))

            self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
            self.response.write(json.dumps(feeds, default=handler))
        else:
            logging.debug("presenting feed list view to /feeds")
            feeds = []
            for feed in feed_results:
                feeds.append(feed)
            template_values = { 'feeds' : feeds }
            template = JINJA_ENVIRONMENT.get_template('feeds/list.html')
            self.response.write(template.render(template_values))

# handler for /feeds/new URI
class NewFeedHandler(webapp2.RequestHandler):
    def get(self):
        logging.debug("presenting feed form to /feeds/new")

        user = users.get_current_user()
        results = db.GqlQuery("SELECT * FROM FeedCategory WHERE user_id = :1", user.user_id())

        feed_categories = []
        for fc in results:
            feed_categories.append(fc)

        template_values = { 'feed' : Feed(), 'feed_categories' : feed_categories }
        template = JINJA_ENVIRONMENT.get_template('feeds/new.html')
        self.response.write(template.render(template_values))

    def post(self):
        logging.info("posting feed form data to /feeds/new")

        result  = self.validateNewFeedData()
        if len(result['errors']) > 0:
            template_values = {
                'message' : "There was a problem with your submission:",
                'messageType' : "error",
                'errors' : result['errors'],
                'feed' : result['feed']
            }
        else:
            logging.info('saving validated new feed')
            result['feed'].put()
            template_values = {
                'message' : "New feed created successfully!",
                'messageType' : "success",
                'feed' : result['feed']
            }

        template = JINJA_ENVIRONMENT.get_template('feeds/new.html')
        self.response.write(template.render(template_values))

    def validateNewFeedData(self):
        logging.info("validating new feed data")
        errors = []

        # basic form validation
        feed_name = self.request.get('feedName')
        if feed_name == '':
            errors.append({ 'field' : 'feedName', 'message' : 'Feed Name cannot be blank.'})
        feed_url = self.request.get('feedUrl')
        if feed_url == '':
            errors.append({ 'field' : 'feedUrl', 'message' : 'Feed URL cannot be blank.'})
        feed_category = self.request.get('feedCategory')

        user = users.get_current_user()

        '''
        if len(errors) == 0:
            # check datastore for existing username
            query = db.GqlQuery("SELECT * FROM User WHERE username = :1", username)
            existing_user = query.get()
            if existing_user and existing_user.username == username:
                errors.append({ 'field' : 'username', 'message' : 'Username already taken.  Please choose another.'})
        '''

        # assemble new feed
        feed = Feed()
        feed.name = feed_name
        feed.feed_url = feed_url
        feed.user_id = user.user_id()

        return { 'errors' : errors, 'feed' : feed }