#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
import jinja2
import logging
import os
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

class RubricListHandler(webapp2.RequestHandler):
    def get(self):

        logging.error("filename => " + os.path.dirname(__file__))

        template_values = {}

        template = JINJA_ENVIRONMENT.get_template('rubrics/list.html')
        self.response.write(template.render(template_values))