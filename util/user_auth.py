#!/usr/bin/env python
#
# Copyright 2013 Greg Heidorn
#
from google.appengine.api import users

@staticmethod
def get_authenticated_user(template_values):
    authenticated_user = {}
    user = users.get_current_user()
    if user:
        authenticated_user.append("username" : user.nickname())
        authenticated_user.append("email" : user.email())
        authenticated_user.append("user_id" : user.user_id())

    #feeds = db.GqlQuery("SELECT * FROM Feed WHERE user_id = :1")

    return authenticated_user